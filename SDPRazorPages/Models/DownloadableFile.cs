﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDPRazorPages.Models
{
    public class DownloadableFile
    {
        public byte[] Content { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}
