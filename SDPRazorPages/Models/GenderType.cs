﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDPRazorPages.Models
{
    public enum GenderType
    {
        Male = 0,
        Female = 1,
        Unclasified = 2
    }
}
