﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDPRazorPages.Models
{
    public class TransferResult
    {
        public string Message { get; set; }
        public ResponseType Status { get; set; }
    }
}
