﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SDPRazorPages.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDPRazorPages.Logic
{
    public class FileTransferLogic
    {
        public async Task<TransferResult> UploadDatabase(IFormFile file,StudentsContext _context)
        {
            if (file != null)
            {
                try
                {
                    _context.Student.RemoveRange(_context.Student); //Patent
                }
                catch (Exception ex)
                {
                    return new TransferResult{ Message = "Błąd przy czyszczeniu bazy: " + ex.Message, Status = ResponseType.Error};
                }

                using (var stream = file.OpenReadStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        var content = reader.ReadToEnd();
                        var newDatabase = JsonConvert.DeserializeObject<List<Student>>(content);
                        _context.Student.AddRange(newDatabase);
                        await _context.SaveChangesAsync();
                    }
                }

                return new TransferResult {Message = "Baza zaktualizowana poprawnie", Status = ResponseType.Ok};
            }
            else
            {
                return new TransferResult { Message = "Proszę wybrać plik", Status = ResponseType.Ok };
            }
        }

        public async Task<DownloadableFile> DownloadDatabase(StudentsContext _context)
        {
            var databaseAsJsonString = JsonConvert.SerializeObject(await _context.Student.ToListAsync()).ToString();
            return new DownloadableFile {Content = Encoding.ASCII.GetBytes(databaseAsJsonString),Type = "application/json", Name = "database.json" };
        }
    }
}
